# FastAPI에서 ORM 사용<sup>[1](#footnote_1)</sup>

이 포스트에서는 보다 유지 관리가 쉽고 효율적인 코드를 작성하기 위해 FastAPI에서 ORM을 설정하고 작업하는 방법에 대해 설명한다.

시간 낭비할 필요 없이 바로 시작하자 🏃‍♂️

## ORM 이란?
객체 관계형 매핑(ORM: Object-Relational Mapping)은 객체 지향 패러다임을 사용하여 데이터 쿼리, 업데이트, 삭제 등 데이터베이스를 조작하는 데 사용되는 프로그래밍 기법이다. Python에는 ORM을 위한 여러 라이브러리가 있지만, 이 튜토리얼에서는 FastAPI와 잘 작동하고 SQLAlchemy과 Pydantic을 동시에 사용할 수 있는 SQLModel을 사용한다.

## Step 1: SQLModel 설치

시작하기 전에 Python 3.6 이상이 설치되어 있는지 확인하여야 한다. 그런 다음 pip를 사용하여 FastAPI와 SQLModel을 설치한다.

```bash
$ pip install fastapi[all] SQLModel
```

## Step 2: 데이터베이스 모델 설정

간단한 모델을 정의하는 것부터 시작해 보겠다. 모델은 데이터베이스 테이블의 구조와 그 관계를 정의하는 Python 클래스이다.

```python
from sqlmodel import SQLModel, Field

class Book(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    title: str
    author: str
    year: int
```

이 `Book` 모델에서 `id`, `title`, `author` 및 `year`는 데이터베이스 테이블의 열을 나타낸다.

## Step 3: 데이터베이스 생성

여기서는 예를 위해 SQLite 데이터베이스를 만들겠다.

```python
from sqlmodel import Session, SQLModel, create_engine

sqlite_url = "sqlite:///books.db"
engine = create_engine(sqlite_url)

def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
```

`create_db_and_tables()`를 실행하면 books 테이블이 있는 새 SQLite 데이터베이스를 생성한다.

## Step 4: 데이터베이스와 상호 작용

이제 데이터베이스가 설정되었으므로 데이터베이스와 상호 작용을 시작할 수 있다. 예를 들어 데이터베이스에 데이터를 삽입하려면 새 Book 인스턴스를 만들어 세션에 추가하면 된다.

```python
def create_book(book: Book):
    with Session(engine) as session:
        session.add(book)
        session.commit()
```

이 함수를 사용하는 방법은 다음과 같다.

```python
new_book = Book(title="1984", author="George Orwell", year=1949)
create_book(new_book)
```

데이터베이스에서 데이터를 가져오는 방법도 간단하다.

```python
def get_books():
    with Session(engine) as session:
        books = session.exec(Book.select()).all()
        return books
```

이 함수는 데이터베이스에 있는 모든 책을 검색한다.

## Step 5: FsstAPI에 통합

FastAPI는 SQLModel과 원할하게 통합된다. 책을 만들고 검색하는 몇 가지 경로를 정의해 보겠다.

```python
from fastapi import FastAPI
app = FastAPI()

@app.post("/books/")
async def create_book_endpoint(book: Book):
    create_book(book)
    return {"message": "Book created"}

@app.get("/books/")
async def get_books_endpoint():
    books = get_books()
    return {"books": books}
```

이게 다이다! SQLModel을 ORM으로 사용하는 간단한 FastAPI 어플리케이션이다.


<a name="footnote_1">1</a>: 이 페이지는 [Using an ORM with FastAPI](https://medium.com/@kasperjuunge/using-an-orm-with-fastapi-7eb6e54f5707)을 편역한 것임.
